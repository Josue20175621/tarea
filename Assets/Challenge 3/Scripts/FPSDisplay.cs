﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSDisplay : MonoBehaviour
{
    private float deltaTime = 0.0f;
    public int target = 100;
    private PlayerControllerX playerControllerScript;
    GUIStyle fpsStyle = new GUIStyle();
    public Texture2D buttonTexture;
    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerControllerX>();
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = target;
    }

    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        if (target != Application.targetFrameRate)
        {
            Application.targetFrameRate = target;
        }
    }

    private void OnGUI()
    {
        int w = Screen.width, h = Screen.height;
        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        fpsStyle.alignment = TextAnchor.UpperLeft;
        fpsStyle.fontSize = h * 2 / 100 + 20;
        fpsStyle.normal.textColor = new Color(1f, 1f, 1f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps) Helio: {2} Tiempo: {3}", msec, fps, playerControllerScript.helium, Time.fixedTime);
        GUI.Label(rect, text, fpsStyle);
    }
}
