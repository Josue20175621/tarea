﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpY : MonoBehaviour
{

    public float speed;
    private PlayerControllerX playerControllerScript;
    private float upperBound = 20;

    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerControllerX>();
    }

    // Update is called once per frame
    void Update()
    {
        // If game is not over, move up
        if (!playerControllerScript.gameOver)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
        }

        // If object goes off screen, destroy it
        if (transform.position.y > upperBound)
        {
            Destroy(gameObject);
        }
    }
}
