﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControllerX : MonoBehaviour
{
    public bool gameOver;

    public float floatForce;
    public readonly float limitY = 17;
    private float gravityModifier = 1.5f;
    private Rigidbody playerRb;

    public ParticleSystem explosionParticle;
    public ParticleSystem fireworksParticle;
    public int helium;
    private AudioSource playerAudio;
    public AudioClip explodeSound;
    public AudioClip gameOverSound;
    public AudioClip shot;
    private AudioSource backgroundMusic;
    public GameObject ballPrefab;
    public float fireRate = 0.5f;
    private float nextFire = 0.0f;
    private MeshRenderer Renderer;

    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity *= gravityModifier;
        playerRb = GetComponent<Rigidbody>();
        playerAudio = GetComponent<AudioSource>();
        Renderer = GetComponent<MeshRenderer>();
        backgroundMusic = GameObject.Find("Main Camera").GetComponent<AudioSource>();

        playerRb.useGravity = false;

    }

    // Update is called once per frame
    void Update()
    {
        // Destroy Spaceship if it flies too high or helium is less than or equal to 0
        if ((transform.position.y > limitY || helium <= 0) && !gameOver)
        {
            playerAudio.PlayOneShot(explodeSound, 1.0f);
            explosionParticle.Play();
            backgroundMusic.Stop();
            Renderer.enabled = false;
            gameOver = true;
        }

        // Subtract 5 to helium level every 10 seconds
        if (Time.fixedTime > 0)
        {
            if (Time.fixedTime % 10 == 0)
            {
                helium -= 5;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if player collides with ground, apply force to the balloon
        if (collision.gameObject.CompareTag("Ground") && !gameOver)
        {
            playerAudio.PlayOneShot(explodeSound, 1.0f);
            explosionParticle.Play();
            backgroundMusic.Stop();
            gameOver = true;
        }

    }

    public void Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            playerAudio.PlayOneShot(shot, 0.1f);
            Instantiate(ballPrefab, transform.position, ballPrefab.transform.rotation);
        }
    }

    public void Fly()
    {
        if (!gameOver && transform.position.y < limitY - 3)
        {
            playerRb.AddForce(Vector3.up * floatForce);
            playerRb.useGravity = true;
        }
    }
}
