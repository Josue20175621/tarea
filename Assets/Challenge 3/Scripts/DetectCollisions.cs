﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisions : MonoBehaviour
{
    private AudioSource playerAudio;
    public AudioClip hit;
    private PlayerControllerX playerControllerScript;
    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerControllerX>();
        playerAudio = GameObject.Find("Player").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        playerAudio.PlayOneShot(hit, 1.0f);
        if (other.gameObject.CompareTag("BlueBalloon") && !playerControllerScript.gameOver)
        {
            playerControllerScript.helium += 10;
        }

        if (other.gameObject.CompareTag("Balloon") && !playerControllerScript.gameOver)
        {
            playerControllerScript.helium += 1;
        }
        Destroy(other.gameObject);
    }

}
